import java.util.ArrayList;


public class TheaterManagement {
double ticketPricesround1[][] ;
double ticketPricesround2[][] ;
double ticketPricesround3[][] ;
	

	public TheaterManagement() {
		
		// TODO Auto-generated constructor stub
	}
	public void createshowtime(double[][] s){
		ticketPricesround1 = new double[15][20];
		ticketPricesround2 = new double[15][20];
		for(int i = 0;i<15;i++){
			for(int j = 0;j<20;j++){
				ticketPricesround1[i][j] = s[i][j];
				ticketPricesround2[i][j] = s[i][j];
				
			}
		}
	}
	public double reserveSeat(String rollStr , int seat){
			seat = seat - 1;
			double prices = -1;
			int roll = getRoll(rollStr);
			if( ticketPricesround1[roll][seat] > 0 ){
				prices = ticketPricesround1[roll][seat];
				ticketPricesround1[roll][seat] = 0;
			}else if( ticketPricesround1[roll][seat] == 0 ){
				prices = 0;
			}
			return prices;
	}
	private int getRoll(String rollStr) {
		// TODO Auto-generated method stub
		int i = rollStr.toUpperCase().charAt(0);
		
		i = i - 65;
		
		return i;
	
	}
	public String getCol(int col){
		
		String colStr = "";
		
		col = col + 65;
		
		char c = (char) col;
		colStr = colStr + c;
		return colStr;
	}
	public void sellSeat(int i , int j){
		
		ticketPricesround1[i][j] = 0;
		
	}
public void showAllSeat(){
		
		String seatNumberLine = "   ";
		for(int i = 0 ; i < ticketPricesround1[0].length ; i++){
			seatNumberLine = seatNumberLine+(i+1)+" ";
			if( i+1 < 10){
				seatNumberLine = seatNumberLine+" ";
			}
		}
		System.out.println(seatNumberLine);
		
		for(int i = 0 ; i < ticketPricesround1.length ;i++){
			
			String line = getCol(i)+"  ";
			
			for(int j = 0 ; j < ticketPricesround1[i].length ; j++){
				
				if( ticketPricesround1[i][j] > 0){
					line = line+"O  ";
				}else if( ticketPricesround1[i][j] == 0){
					line = line+"X  ";
				}else{
					line = line+"   ";
				}
				
				
				
			}
			System.out.println(line);
			
		}
		
		System.out.println("X = reserved ,\tO = free\n\n");
		
	}
public String searchFreeSeatBySeatPrices(double seatPrices){
	
	String s = "";
	
	for(int i = 0 ; i < ticketPricesround1.length ; i++){
		String line = "";
		line = line + getCol(i)+" ";
		int foundFlag = 0;
		for(int j = 0 ; j < ticketPricesround1[i].length ; j++ ){
			
			if(  ticketPricesround1[i][j] == seatPrices){
				line = line + (j+1) +" ";
				foundFlag = 1;
			}
			
		}
		if(foundFlag == 1){
			line = line+"\n";
			s = s+line;
		}
		
		
	}
	
	return s;
}

	public int[][] searchIndexSeatBySeatPrices(double seatPrices){
	
		ArrayList<Integer> iL = new ArrayList<>();
		ArrayList<Integer> jL = new ArrayList<>();
		for(int i = 0 ; i < ticketPricesround1.length ; i++){
			
			for(int j = 0 ; j < ticketPricesround1[i].length ; j++ ){
				
				
				if(  ticketPricesround1[i][j] == seatPrices){
					
					iL.add(i);
					jL.add(j);
				}
				
			}
			
		}
	
	
			int[][] ij = new int[iL.size()][2];
			for(int i = 0 ; i < iL.size() ; i++){
				
				ij[i][0] = iL.get(i);
				ij[i][1] = jL.get(i);
		
	}
	
			return ij;
	
}

public double[][] getTicketPrices() {
	return ticketPricesround1;
}

}
	

